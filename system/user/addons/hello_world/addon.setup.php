<?php

return array(
    'author'      => 'Ken Nguyen',
    'author_url'  => 'http://kennjdemo.com',
    'name'        => 'Hello World',
    'description' => 'Displays a friendly "Hello world!" message.',
    'version'     => '3.0.0',
    'namespace'   => 'User\Addons\HelloWorld'
);