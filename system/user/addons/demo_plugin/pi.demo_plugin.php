<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Demo_plugin
{
    public $return_data = "";

    public function __construct()
    {
        $this->return_data = ee()->TMPL->tagdata;
    }


    /**
     * Get All Posts from RSS
     * @return array
     */
    public function get_posts()
    {
        $xml = simplexml_load_file('http://paleosun.com/blog/rss/', 'SimpleXMLElement', LIBXML_NOCDATA);
        //$xml = simplexml_load_file('http://express.dev/blog_rss_.xml', 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = [];
        $count = 0;
        foreach ($xml->channel->item as $key => $item) {
            $data[] = [
                'title' => $item->title[0],
                'description' => $item->description[0]
            ];
            $count++;
            if ($count === 4) {
                break;
            }
        }
        return $data;
    }

    /**
     * Return list of posts
     * @return mixed
     */
    public function show_posts()
    {
        $vars[]['posts'] = $this->get_posts();
        return ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $vars);
    }

}