<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['debug'] = '1';
$config['enable_devlog_alerts'] = 'n';
$config['cache_driver'] = 'file';
// ExpressionEngine Config Items
// Find more configs and overrides at
// https://docs.expressionengine.com/latest/general/system_configuration_overrides.html

$config['app_version'] = '3.4.6';
$config['encryption_key'] = '6a6e24f5a79baa5e3e1f16b123538700f8633ef8';
$config['database'] = array(
	'expressionengine' => array(
		'hostname' => 'localhost',
		'database' => 'paleosun',
		'username' => 'root',
		'password' => '123qwe',
		'dbprefix' => 'exp_',
		'port'     => ''
	),
);

// EOF