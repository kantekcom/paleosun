<?php
return array(
    'author'      => 'Ken Nguyen',
    'name'        => 'Demo Plugin',
    'description' => 'Displays a list of posts from RSS.',
    'version'     => '1.0.0',
    'namespace'   => 'User\Addons\DemoPlugin'
);